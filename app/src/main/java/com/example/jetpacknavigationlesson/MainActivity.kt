package com.example.jetpacknavigationlesson

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController // Переменная navController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        /**
         * Получить NavHostFragment
         * R.id.fragment_container идентификатор разметки activity_main
         * это необходимо для запуска статической функции setupActionBarWithNavController
         * класса NavigationUI
         */
        val navHost = supportFragmentManager.findFragmentById(R.id.fragment_container) as NavHostFragment
        navController = navHost.navController

        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    /**
     * если кнопка не отработала возврат на предыдущее меню
     * то отрабатывает super.onSupportNavigateUp()
     * ниже запись сокращенный вариант if/else
     */
    override fun onSupportNavigateUp(): Boolean { //Функция обработчика системной кнопки ActionBar
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}