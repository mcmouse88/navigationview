package com.example.jetpacknavigationlesson

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.jetpacknavigationlesson.databinding.FragmentRootBinding

class RootFragment: Fragment(R.layout.fragment_root) {
    private lateinit var binding: FragmentRootBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentRootBinding.bind(view)
        binding.greenBoxButton.setOnClickListener {
            openBox(Color.GREEN, getString(R.string.green))
        }

        binding.yellowBoxButton.setOnClickListener {
            openBox(Color.YELLOW, getString(R.string.yellow))
        }

        val liveDate = findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Int>(BoxFragment.EXTRA_RANDOM_NUMBER)
        liveDate?.observe(viewLifecycleOwner) {
            randomNumber ->
            if(randomNumber != null) {
                Toast.makeText(
                    requireContext(),
                    "Generated number: $randomNumber",
                    Toast.LENGTH_SHORT
                ).show()
                liveDate.value = null
            }
        }
    }

    private fun openBox(color: Int, colorName: String) {
        val direction = RootFragmentDirections.actionRootFragmentToBoxFragment(color, colorName)

        findNavController().navigate(
            direction,
            navOptions {
                anim {
                    enter = R.anim.enter
                    exit = R.anim.exit
                    popEnter = R.anim.pop_enter
                    popExit = R.anim.pop_exit
                }
            }
        )
    }
}