package com.example.jetpacknavigationlesson

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.NavArgs
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.jetpacknavigationlesson.databinding.FragmentBoxBinding
import kotlin.random.Random

class BoxFragment: Fragment(R.layout.fragment_box) {
    private lateinit var binding: FragmentBoxBinding
    private val args: BoxFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentBoxBinding.bind(view)
        val color = args.color // Принять данные с другого Activity
        binding.root.setBackgroundColor(color)

        binding.buttonGoBack.setOnClickListener {
            findNavController().popBackStack() // Вернуться назад по навигации
        }

        binding.buttonOpenSecret.setOnClickListener {
            findNavController().navigate(BoxFragmentDirections.actionBoxFragmentToSecretFragment())
        }

        binding.buttonGenerateNumber.setOnClickListener {
            val number = Random.nextInt(100)
            findNavController().previousBackStackEntry?.savedStateHandle?.set(EXTRA_RANDOM_NUMBER, number) // Передача результата на предыдущий экран
            findNavController().popBackStack() // Возврат на предыдущий экран
        }
    }

    companion object {
        const val EXTRA_RANDOM_NUMBER = "EXTRA_RANDOM_NUMBER"
    }
}